package es.lab.project;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.context.annotation.Scope;

import lombok.Getter;
import lombok.Setter;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class MyData {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String origin_country;
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
    }
    public void setOrigin_country(String origin_country){
        this.origin_country = origin_country;
    }
    public String getOrigin_country(){
        return this.origin_country;
    }
    
    private String time;
    
    public MyData() {
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "MyData{" +
                "id='"+ id+ '\''+
                ", time='" + time +
                 '\'' +
                 ", origin_country='" + origin_country +
                 '\'' +
                '}';
    }
}