import React, { Component } from 'react';
import StyledRoomWrapper from './Room.style';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';
import moment from 'moment';
import {Input, Button, Divider} from 'antd';

const possibleAcTemps = [0, 16, 17, 18, 19, 20, 21, 22, 23, 24];
let temp = 16;

class Room extends Component {
    state = {
        data:[],
        range: 24,
    }

    data = (timestamp, ac = null, motionDetector = null) => {
        let acTemp = possibleAcTemps[Math.floor(Math.random() * possibleAcTemps.length)];
        if(ac){
            if(ac>temp)
                temp+=ac/100
            else
                temp-=ac/100
        }else 
            if(acTemp!=0){
                if(acTemp>temp)
                    temp+=acTemp/100
                else
                    temp-=acTemp/100
            }
            
        return {
            name: moment(timestamp*1000).format("DD-MM-YYYY HH:mm:ss"),
            ac: ac ? ac : acTemp ,
            motionDetector: motionDetector ? 1: Math.floor((Math.random() * 2)),
            temp: temp.toFixed(2),
        }
    }
    
    componentWillMount() {
        const timestamp = Math.floor(Date.now() / 1000);
        let  data = [];
        for( var toIncrement = timestamp - ( 3600 * this.state.range); toIncrement < timestamp ; toIncrement+=600){
            data.push(this.data(toIncrement));
        } 
        this.setState({data: data, timestamp:timestamp});
    }

    componentDidMount(){
        this.interval = setInterval(() => {
            const timestamp = Math.floor(Date.now() / 1000);
            this.setState({ data: [...this.state.data.slice(1), this.data(timestamp)], timestamp: timestamp })
        }, 10000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }
    
    render() {
        let i=0, total=0, electricityCost=0;
        this.state.data.map( el => { 
            total+=+ el.temp; i++
            electricityCost+= el.ac !== 0 ? +el.ac*(0.05/6) : 0
        })
        return (
            <StyledRoomWrapper>
                <span className="title">Last updated at {moment(this.state.timestamp*1000).format("DD-MM-YYYY HH:mm:ss")}</span>
                <br/>Average temp : {Number(total/i).toFixed(2)}
                <br/>Electricity Cost last {this.state.range}height : {electricityCost.toFixed(2)} €
                <span className="card">
                    <LineChart
                        width={850}
                        height={300}
                        data={this.state.data}
                        margin={{
                            top: 5, right: 30, left: 20, bottom: 5,
                        }}
                        
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Line type="monotone" dataKey="temp" stroke="#8884d8" activeDot={{ r: 0.5 }}  /> 
                        <Line type="monotone" dataKey="ac" stroke="#82ca9d" activeDot={{ r: 0.5 }}  /> 

                        {/* activeDot={{ r: 8 }} */}
                    </LineChart>
                </span>
                <Divider/>
                <span className="chart">
                    <LineChart
                        width={850}
                        height={150}
                        data={this.state.data}
                        margin={{
                            top: 30, right: 30, left: 20, bottom: 5,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend  style={{marginTop:20}}/>
                        <Line type="monotone" dataKey="motionDetector" stroke="#82ca9d" /> 
                    </LineChart>
                </span>
                <Input placeholder="AC Temp" value={this.state.ac} onChange={(e) => this.setState({ac:e.target.value})}/>
                <Button onClick={() => this.setState({data: [...this.state.data, this.data(Math.floor(Date.now() / 1000),this.state.ac)], ac: null})}>Inserir Temperatura</Button>
                <Button onClick={() => this.setState({data: [...this.state.data, this.data(Math.floor(Date.now() / 1000),null,1)]})}>Simular Movimento</Button>


            </StyledRoomWrapper>

        );
    }
}

export default Room;

// (function () {
//     setInterval(function () {
//         axios.get('/route/to/partial/view',)
//             .then(function(response){
//                     document.querySelector('#partial')
//                             .innerHtml(response.data);
//             }); // do nothing for error - leaving old content.
//         }); 
//     }, 60000); // milliseconds
// })();