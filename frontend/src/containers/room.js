import React, { Component } from 'react';
import LayoutContentWrapper from '../components/utility/layoutWrapper';
import LayoutContent from '../components/utility/layoutContent';
import Room from './Room/Room';

export default class extends Component {
  render() {
    return (
      <LayoutContentWrapper style={{ height: '100%' }}>
        <LayoutContent>
          <Room />
        </LayoutContent>
      </LayoutContentWrapper>
    );
  }
}
