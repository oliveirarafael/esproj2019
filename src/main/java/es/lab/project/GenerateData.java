
package es.lab.project;

import java.util.Random;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Dummy data generator. Used to public dummy sensor data directly into kafka.
 */
public class GenerateData {

    private String name = "foo";
    private Double ac, temp;
    private Boolean motionDetector;
    private int[] possibleAcTemps = {0, 16, 17, 18, 19, 20, 21, 22, 23, 24};

    public GenerateData(double ac, boolean motionDetector){
        this.ac=this.temp=ac;
        this.motionDetector=motionDetector;
    }

    public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
    }

    public void setAC(Double ac){
        this.ac = ac;
    }

    public double getAC(){
        return this.ac;
    }

    public void generateTemp(){
        Random rnd = new Random();
        int acTemp = possibleAcTemps[rnd.nextInt(possibleAcTemps.length)];
        
        if(ac!=0){
            if(ac>temp){
                temp+=ac/100;
            }else{
                temp-=ac/100;
            }
        }else{
            if(acTemp!=0){
                if(acTemp>temp)
                    temp+=acTemp/100;
                else
                    temp-=acTemp/100;
            }
        }
      
    }
    public double getTemp(){
        return this.temp;
    }
    
    public void setMotionDetector(Boolean state){
        this.motionDetector = state;
    }

    public Boolean getMotionDetector(){
        return this.motionDetector;
    }


    @Override
    public String toString() {
        return  "{" +
                "name:'"+ name+ '\''+
                ", ac:'" + ac + '\'' +
                ", motionDetector:'" + motionDetector + '\'' +
                ", temp:'" + temp + '\'' +
                '}';
    }
}