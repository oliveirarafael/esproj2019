import styled from 'styled-components';
import WithDirection from '../../settings/withDirection';
import { palette } from 'styled-theme';

const SidebarWrapper = styled.div`
padding: 40px 20px;
display: flex;
flex-flow: row wrap;
overflow: hidden;

@media only screen and (max-width: 767px) {
  padding: 50px 20px;
}

@media (max-width: 580px) {
  padding: 15px;
}


.card{
  width: 100%;
  padding: 35px;
  background-color: #e9ebf1;
  border: 1px solid ${palette('border', 2)};
  height: 100%;
}

.chart{
  margin-top: 30px;
}
`;

export default WithDirection(SidebarWrapper);
