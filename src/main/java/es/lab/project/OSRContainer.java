package es.lab.project;

public class OSRContainer {

    private OpenSkyRest osr;
    private MyData myData;
    public OSRContainer() {
    }

    public void setOSR(OpenSkyRest o){
        this.osr = o;
    }

    public OpenSkyRest getOSR(){
        return this.osr;
    }

    public MyData getMyData(){
        return this.myData;
    }

    public void setMyData(MyData myData){
        this.myData = myData;
    }

}