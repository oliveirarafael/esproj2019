package es.lab.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate; /* added */

import java.util.ArrayList;
import java.util.Random;

import org.slf4j.Logger; /* added */
import org.slf4j.LoggerFactory; /* added */
import org.springframework.boot.web.client.RestTemplateBuilder; /* added */
import org.springframework.context.annotation.Bean; /* added */
import org.springframework.context.annotation.Scope;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.beans.factory.annotation.Autowired;

@SpringBootApplication
@EnableScheduling
public class ProjectApplication {
	private static final Logger log = LoggerFactory.getLogger(ProjectApplication.class);
	@Autowired private OpenSkyRest osr;
	@Autowired private OSRContainer osrContainer;
	@Autowired private RestTemplate restTemplate;
	@Autowired private OSRSRepo osrsRepo;
	@Autowired private MyData myData;
	@Autowired private Sender sender; 
	@Autowired private GenerateData dataGenerator;
	@Autowired private Random rnd;
	
	
	public static void main(String[] args) {
		SpringApplication.run(ProjectApplication.class, args);
	}

	
	@Bean @Autowired
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
	
	@Bean
	public OpenSkyRest openSkyRest(RestTemplate restTemplate) {
		return restTemplate.getForObject(
				"https://opensky-network.org/api/states/all", OpenSkyRest.class);
	}
	@Bean
	public MyData myDataFn() {
		return new MyData();
	}
	@Bean public Random newRnd(){
		return new Random();
	}
	
	@Bean @Scope("singleton")
	public OSRContainer oContainer() {
		return new OSRContainer();
	}

	@Bean
	public GenerateData genData(){
		return new GenerateData(20.0, false);
	}
	/*
	@Scheduled(fixedDelay=10000)
	public void updater_rest(){
		osr = restTemplate.getForObject(
				"https://opensky-network.org/api/states/all", OpenSkyRest.class);
		//log.info(osr.toString());
		osrContainer.setOSR(osr);
		// log.info(osr.getStates().toString());
		for( ArrayList<String> a : osr.getStates()){
			if(a.contains("United States")){
				// log.info(a.toString());
				// log.info("hit");
				sender.send(a.toString());
			}
		}
		//myData.setTime(osr.getTime());
		//myData.setOrigin_country(osr.getStates().);
		//osrsRepo.save();
		
	}
	*/

	@Scheduled(fixedDelay = 1000)
	public void newDummyData(){
		
		dataGenerator.generateTemp();
		dataGenerator.setMotionDetector(rnd.nextFloat()>0.95?true:false);
		sender.send(dataGenerator.toString());
	}
	

	
}

