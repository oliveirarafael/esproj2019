package es.lab.project.web;

import org.slf4j.Logger; /* added */
import org.slf4j.LoggerFactory; /* added */
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.slf4j.Logger; /* added */
import org.slf4j.LoggerFactory; /* added */
import es.lab.project.OpenSkyRest;
import es.lab.project.ProjectApplication;

/* check this later: https://spring.io/guides/gs/serving-web-content/
    for extra customization
*/
import es.lab.project.OSRSRepo;
import es.lab.project.OpenSkyRest;
@Controller
@CrossOrigin
public class HomeController {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired private OpenSkyRest osr;
    //@Autowired private Logger log;
    @Autowired private OSRSRepo osrsRepo;

    /*
        separar o opensky, pondo-o num sitio como shared resource e atualiza-lo frequentemente
        ver @schedule tag ou la como se escreve.
        Vogella para jpa ? recommended by prof
    
    */
    @GetMapping("/home")
    public String home(Model model) { /*context variable */
        
        Logger log = LoggerFactory.getLogger(ProjectApplication.class);
        model.addAttribute("time", osr.getTime());
        return "home";
    }
    /*
    @GetMapping(path="/all")
	public @ResponseBody Iterable<OpenSkyRest> getAllOpenSkyRest() {
		// This returns a JSON or XML with the users
		return osrsRepo.findAll();
    }
    */
    /*
    @GetMapping(path="/time/{minutes}")
    public @ResponseBody Iterable<OpenSkyRest> getFromLastMinutes(@PathVariable String minutes) {
        //Iterable<OpenSkyRest> tmp =osrsRepo.getAllFromLastMinutes(minutes);
        //log.info(tmp.toString());
        return osrsRepo.getAllFromLastMinutes(minutes);
    }   
    */
}