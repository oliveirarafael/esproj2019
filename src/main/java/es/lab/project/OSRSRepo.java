package es.lab.project;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

/*
@RepositoryRestResource(collectionResourceRel = "time", path = "time")
public interface OSRSRepo extends PagingAndSortingRepository<OpenSkyRest, Long>{

    List<OpenSkyRest> findByTime(@Param("time") String time);
}*/

public interface OSRSRepo extends CrudRepository<MyData, Integer> {
    /*
    @Query(value = "select * from open_sky_rest t where t.time > ?1", nativeQuery=true)
    public List<OpenSkyRest> getAllFromLastMinutes(@Param("minutes")String minutes);
    */
}