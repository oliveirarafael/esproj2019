package es.lab.project;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import org.springframework.context.annotation.Scope;


@JsonIgnoreProperties(ignoreUnknown = true)
public class OpenSkyRest {
   
   
    
    private List<ArrayList<String>> states =  new ArrayList<ArrayList<String>>();
    

    private String time;
    /*private State states;*/

    public OpenSkyRest() {
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public List<ArrayList<String>> getStates() {
        return this.states;
    }
    public void setStates(List<ArrayList<String>> states){
        this.states = states;
    }

    /*public void setStates(State[] states) {
        this.states = states;
    }*/

    @Override
    public String toString() {
        return "OpenSkyRest{" +
                // "id='"+ '\''+
                "time='" + time +
                 '\'' +
                '}';
    }
}