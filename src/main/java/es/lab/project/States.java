package es.lab.project;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.*;


public class States {

    
   String icao24;
   /*@Getter @Setter private String callsign;
   @Getter @Setter private String origin_country;
   @Getter @Setter private float longitude;
   @Getter @Setter private int time_position;
   @Getter @Setter private float latitude;
   @Getter @Setter private float baro_altitude;
   @Getter @Setter private boolean on_ground;
   @Getter @Setter private float velocity;
   @Getter @Setter private float true_track;
   @Getter @Setter private float vertical_rate;
   @Getter @Setter private int[] sensors;
   @Getter @Setter private float geo_altitude;
   @Getter @Setter private String squawk;
   @Getter @Setter private boolean spi;
   @Getter @Setter private int position_source;*/

    public States() {
    }
    public void setIcao24(String icao24){
        this.icao24 = icao24;
    }
    public String getIcao24(){
        return this.icao24;
    }

    
    @Override
    public String toString() {
        return "State{" +
                "icao24=" + icao24 +
                ", callsign='"  + '\'' +
                '}';
    }

}
