package es.lab.project;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
@JsonIgnoreProperties(ignoreUnknown = true)
public class State {

   @Getter private String icao24;
   @Getter private String callsign;
   /*@Getter private String origin_country;
   @Getter private float longitude;
   @Getter private int time_position;
   @Getter private float latitude;
   @Getter private float baro_altitude;
   @Getter private boolean on_ground;
   @Getter private float velocity;
   @Getter private float true_track;
   @Getter private float vertical_rate;
   @Getter private int[] sensors;
   @Getter private float geo_altitude;
   @Getter private String squawk;
   @Getter private boolean spi;
   @Getter private int position_source;*/

    public State() {
    }

    @Override
    public String toString() {
        return "State{" +
                "icao24=" + icao24 +
                ", callsign='" + callsign + '\'' +
                '}';
    }

}